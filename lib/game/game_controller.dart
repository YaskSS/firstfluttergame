import 'dart:math';
import 'dart:ui';

import 'package:demo_game/game/components/enemy.dart';
import 'package:demo_game/game/components/enemy_spawn.dart';
import 'package:demo_game/game/components/health_bar.dart';
import 'package:demo_game/game/components/high_score_text.dart';
import 'package:demo_game/game/components/player.dart';
import 'package:demo_game/game/components/score_bar_text.dart';
import 'package:demo_game/game/components/start_button.dart';
import 'package:demo_game/game/state.dart';
import 'package:flame/flame.dart';
import 'package:flame/game/base_game.dart';
import 'package:flame/gestures.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class GameController extends BaseGame with TapDetector {
  static const String KEY_HIGH_SCORE = 'highScoreKey';

  Size screenSize;
  double tileSize;
  int score;

  Random random;
  Player player;
  List<Enemy> enemies;
  HealthBar healthBar;
  EnemySpawner enemySpawner;
  ScoreBarText scoreBarText;
  GameState state;
  HighScoreText highScoreText;
  StartTextButton startTextButton;

  final SharedPreferences storage;

  GameController(this.storage) {
    initialize();
  }

  void initialize() async {
    resize(await Flame.util.initialDimensions());
    state = GameState.MENU;
    score = 0;
    scoreBarText = ScoreBarText(this);
    random = Random();
    player = Player(this);
    enemies = List<Enemy>();
    enemySpawner = EnemySpawner(this);
    healthBar = HealthBar(this);
    highScoreText = HighScoreText(this);
    startTextButton = StartTextButton(this);
  }

  @override
  void render(Canvas canvas) {
    super.render(canvas);
    Rect background = Rect.fromLTWH(0, 0, screenSize.width, screenSize.height);
    Paint backgroundPaint = Paint()..color = Colors.blue;
    canvas.drawRect(background, backgroundPaint);
    player.render(canvas);

    if (state == GameState.MENU) {
      highScoreText.render(canvas);
      startTextButton.render(canvas);
    } else {
      enemies.forEach((enemy) {
        enemy.render(canvas);
      });
      scoreBarText.render(canvas);
      healthBar.render(canvas);
    }
  }

  @override
  void update(double t) {
    if (state == GameState.MENU) {
      highScoreText.update(t);
      startTextButton.update(t);
    } else if (state == GameState.PLAYING) {
      enemySpawner.update(t);
      enemies.forEach((enemy) {
        enemy.update(t);
      });
      enemies.removeWhere((enemy) => enemy.isDead);
      player.update(t);
      scoreBarText.update(t);
      healthBar.update(t);
    }
  }

  @override
  void resize(Size size) {
    super.resize(size);
    screenSize = size;
    tileSize = screenSize.width / 10;
  }

  @override
  void onTapUp(TapUpDetails details) {
    if (state == GameState.MENU) {
      state = GameState.PLAYING;
    } else {
      enemies.forEach((enemy) {
        if (enemy.enemyRect.contains(details.globalPosition)) {
          enemy.onTapUp();
        }
      });
    }
  }

  void spawnEnemy() {
    double x, y;

    switch (random.nextInt(4)) {
      case 0:
        // Top
        x = random.nextDouble() * screenSize.width;
        y = -tileSize * 2.5;
        break;
      case 1:
        // Right
        x = screenSize.width + tileSize * 2.5;
        y = random.nextDouble() * screenSize.height;
        break;
      case 2:
        // Bottom
        x = random.nextDouble() * screenSize.width;
        y = screenSize.height + tileSize * 2.5;
        break;
      case 3:
        // Left
        x = -tileSize * 2.5;
        y = random.nextDouble() * screenSize.height;
        break;
    }
    enemies.add(Enemy(this, x, y));
  }
}
