import 'package:demo_game/game/game_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Player {
  bool isDead = false;

  int maxHealth;
  int currentHealth;

  final GameController gameController;
  Rect playerRect;

  Player(this.gameController) {
    currentHealth = maxHealth = 300;
    final size = gameController.tileSize * 1.5;
    playerRect = Rect.fromLTWH(gameController.size.width / 2 - size / 2,
        gameController.size.height / 2 - size / 2, size, size);
  }

  void render(Canvas canvas) {
    Paint color = Paint()..color = Colors.pinkAccent;
    canvas.drawRect(playerRect, color);
  }

  void update(double t) {
    if(!isDead && currentHealth <=0){
      isDead = true;
      gameController.initialize();
    }
  }
}
