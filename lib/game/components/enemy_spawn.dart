

import 'dart:core';

import 'package:demo_game/game/game_controller.dart';

class EnemySpawner {
     final GameController gameController;
     final int minSpawnInterval = 700;
     final int maxSpawnInterval = 3000;
     final int maxEnemies = 7;
     final int intervalChange = 3;
     int currentInterval;
     int nextSpawn;

     EnemySpawner(this.gameController){
       initialize();
     }

     void initialize(){
       killAll();
       currentInterval = maxSpawnInterval;
       nextSpawn = DateTime.now().millisecondsSinceEpoch + currentInterval;
     }

     void killAll(){
       gameController.enemies.forEach((enemy) { enemy.isDead = true; });
     }

     void update(double t){
       int currentTime = DateTime.now().millisecondsSinceEpoch;
       if(gameController.enemies.length < maxEnemies && currentTime >= nextSpawn){
          gameController.spawnEnemy();

          if(currentInterval > minSpawnInterval){
            currentInterval -= intervalChange;
            currentInterval -= (currentInterval * 0.1).toInt();
          }

          nextSpawn = currentTime + currentInterval;
       }
     }
}