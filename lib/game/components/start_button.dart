
import 'package:demo_game/game/game_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class StartTextButton{
  final GameController gameController;
  TextPainter textPainter;
  Offset position;

  StartTextButton(this.gameController){
    textPainter = TextPainter(textAlign:  TextAlign.center, textDirection: TextDirection.ltr);
    position = Offset.zero;
  }

  void render(Canvas canvas){
    textPainter.paint(canvas, position);
  }

  void update(double t){
    int score  = gameController.storage.getInt(GameController.KEY_HIGH_SCORE) ?? 0;

    textPainter.text = TextSpan(
        text: 'Start game',
        style: TextStyle(color:  Colors.black, fontSize: 50.0)
    );

    textPainter.layout();

    position = Offset((gameController.screenSize.width /2) - textPainter.width /2,
        gameController.screenSize.height * 0.7  - (textPainter.height / 2));
  }
}